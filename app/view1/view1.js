'use strict';
angular.module('myApp.view1', ['ngRoute','ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])
    .controller('View1Ctrl', function($scope) {
        $scope.result = function() {
            if ($scope.operator === 'KVA') {
                return Math.pow($scope.a,2);
            }
            if ($scope.operator === 'KRA') {
                return Math.sqrt($scope.a).toFixed(2);
            }
            if ($scope.operator === 'KVAB') {
                return Math.pow($scope.a,2)-Math.pow($scope.b,2);
            }
            if ($scope.operator === 'A+B') {
                return $scope.a + $scope.b;
            }
            if ($scope.operator === 'A*B') {
                return $scope.a * $scope.b;
            }
        };
    });